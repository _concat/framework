<?php

namespace Concat\Framework;

use \Concat\Framework\Templating\TwigEngine;
use \Concat\Auth\Authenticator;
use \Concat\Http\Request;
use \Concat\Http\Response;
use \Concat\Routing\Router;

class Application {

    //
    private $router;

    //
    private $request;

    //
    private $twig;

    //
    private $config;

    //
    private $auth;


    public function __construct($config){

        $this->config = $this->parseConfig($config);
    }

    private function parseConfig($config){

        $paths = $config['paths'];

        foreach($paths as $key => $path){
            $config['paths'][$key] = rtrim($path, '/') . "/";
        }

        var_dump($config);

        return $config;
    }

    public function run(){

        $request = $this->getRequest();

        $uri = $request->getURI();

        $method = $request->getMethod();

        return $this->dispatch($uri, $method);
    }

    public function getTemplatingEngine(){

        if($this->$twig === null){
            $this->$twig = new TwigEngine($this->config);
        }
        return $this->$twig;
    }

    public function getRouter(){

        $routes = [];

        $spec = $config['paths']['routes'];

        if(is_array($spec)){
            foreach($spec as $path){
                $routes = array_merge($routes, require($path));
            }
        } else {
            $routes = require($spec)
        }

        return $this->load('router', 'Router', $routes);
    }

    public function getRequest(){
        return $this->load('request', 'Request');
    }

    public function getConfig(){
        return $this->config;
    }

    public function getAuthenticator(){

        $server = new SQLiteStorageProvider();

        $client = new CookieStorage();

        return $this->load('auth', 'Authenticator', $server, $client);
    }

    public function getAuthenticator(){
        return $this->auth;
    }

    private function load($property, $class){
        if($this->$property === null){
            $this->$property = new $class(...$args);
        }
        return $this->$property;
    }



    private function dispatch($uri, $method){

        $router = $this->getRouter();

        $response = $router->dispatch($uri, $method, $this);

        if(is_subclass_of($response, Response::class)){
            return $response->send();
        }

        echo $response;
    }
}
