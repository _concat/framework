<?php

namespace Concat\Framework\Events;

class EventHandler
{
    private $events = [];

    public function register($name, callable $action)
    {
        $this->events[$name] = $action;
    }

    public function unregister($name)
    {
        unset($this->events[$name]);
    }

    public function trigger($name, ...$args)
    {
        if (isset($this->events[$name])) {
            return $this->events[$name](...$args);
        }
    }
}
