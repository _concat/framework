<?php

namespace Concat\Framework\Http;

use \Concat\Http\Response;

class RenderResponse extends Response {


    private $template;
    private $context;
    private $app;

    public function __construct($app, $template, $context){
        $this->template = $template;
        $this->context = $context;
        $this->app = $app;
    }

    public function getContent(){
        return $this->app->getRenderingEngine()->render(
            $this->template,
            $this->context,
        );
    }
}
