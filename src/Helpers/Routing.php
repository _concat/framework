<?php

function _create_route_destination_callback($name, $namespace)
{
    return function ($values, $app) use ($name) {

        // replace filesystem slashes with namespace slashes
        $name = ltrim(str_replace("/", "\\", $name), "/");

        // prepend project view namespace
        $class = "\\App\\$namespace\\$name";

        // instantiate the view
        $destination = new $class($app, ...$values);

        // method that we routed with
        $method = $app->getRequest()->getMethod();

        // call the method that we routed with
        return $destination->$method($app, ...$values);
    };
}

function view($view)
{
    return _create_route_destination_callback($view, "Views");
}

function rest($endpoint)
{
    return _create_route_destination_callback($endpoint, "Endpoints");
}
