<?php

namespace Concat\Framework\Views;

/**
* - Define the context to be rendered
* - Determine the template to present the context
* -
*/
abstract class View
{
    protected function redirect($app, $url)
    {
        // return a redirect response
        return new RedirectResponse($app, $url);
    }

    protected function render($app, $template, $context)
    {
        // return a render response
        return new RenderResponse($app, $template, $context);
    }
}
